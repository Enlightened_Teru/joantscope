package com.tesm.exercise;

import org.junit.Assert;
import org.junit.Test;


public class MatrixTest {
    @Test
    public void givenMatrixShouldReturnReversedXMatrix() throws InvalidInputException {
        //reverseX
        Matrix matrix = new Matrix(
                "+ \n"+" +\n",0,0,2,2);
        String expected = "01\n10\n";
        Assert.assertEquals(expected,matrix.reverseX().toString());
    }

    @Test
    public void givenMatrixShouldReturnReversedYMatrix() throws InvalidInputException {
        //reverseY
        Matrix matrix = new Matrix(
                "+ \n"+" +\n",0,0,2,2);
        String expected = "01\n10\n";
        Assert.assertEquals(expected,matrix.reverseY().toString());
    }

    @Test
    public void givenMatrixShouldReturnRotated90Matrix() throws InvalidInputException {
        //rotate Sx
        Matrix matrix = new Matrix(
                "+ \n"+" +\n",0,0,3,2);
        String expected = "10\n01\n";
        Assert.assertEquals(expected,matrix.rotateSx().toString());
    }

    @Test
    public void givenStringShouldReturnListOfIntegers() throws InvalidInputException {
        //List<integers> getLine(String line)
        //it converts ' ' to 0 and '+' to 1 and puts them in a list
        Matrix matrix = new Matrix(
                "+ \n"+
                " +\n",0,0,2,2);
        String expected = "10\n01\n";
        Assert.assertEquals(expected,matrix.toString());
    }

    //Should i test wrong values
    @Test
    public void givenRightMatrixDimensionsShouldReturnTrue()  {
        try {
            Matrix matrix = new Matrix(
                    "+ \n"+
                    " +\n",0,0,2,2);
        } catch (InvalidInputException e) {
            Assert.assertTrue(false);
            return;
        }
        Assert.assertTrue(true);
    }

    @Test
    public void givenWrongMatrixDimensionsShouldReturnTrue()  {
        //It throws an exception
        try {
            Matrix matrix = new Matrix(
                    "+ \n"+
                            " +\n",0,0,-2,-2);
        } catch (InvalidInputException e) {
            Assert.assertTrue(true);
            return;
        }
        Assert.assertTrue(false);
    }

    @Test
    public void given2EqualMatrixShouldReturnTrue() throws InvalidInputException {
        Matrix matrix = new Matrix("+ \n"+
                        " +\n",0,0,2,2);

        Matrix matrix1 = new Matrix(
                "+ \n"+" +\n",0,0,2,2);

        Assert.assertTrue(matrix.equals(matrix1));
    }


    @Test
    public void given2EqualMatrixShouldReturn100() throws InvalidInputException {
        Matrix matrix = new Matrix("+ \n"+
                " +\n",0,0,2,2);

        Matrix matrix1 = new Matrix(
                "+ \n"+" +\n",0,0,2,2);

        Assert.assertEquals(100,matrix.compareTo(matrix1),0);
    }

    @Test
    public void given2DifferentSizeMatrixShouldReturn0() throws InvalidInputException {
        Matrix matrix = new Matrix("+ \n"+
                " +\n",0,0,2,2);

        Matrix matrix1 = new Matrix(
                "+  \n"+" + \n"+"  +\n",0,0,3,3);

        Assert.assertEquals(0,matrix.compareTo(matrix1),0);
    }


}
