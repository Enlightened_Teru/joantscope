package com.tesm.exercise;

import org.junit.Assert;
import org.junit.Test;


public class VectorTest {

    @Test
    public void given2EqualVectorsShouldReturnTrue(){
        Vector vector = new Vector();
        Vector vector1 = new Vector();

        vector.add(3);
        vector1.add(3);

        Assert.assertTrue(vector.equals(vector1));
    }

    @Test
    public void given2EqualVectorsShouldReturn1(){
        Vector vector = new Vector();
        Vector vector1 = new Vector();

        vector.add(-1);
        vector.add(3);
        vector.add(4);
        vector1.add(-1);
        vector1.add(3);
        vector1.add(4);

        Assert.assertEquals(1,vector.compareTo(vector1),0.00000002);
    }

    @Test
    public void given2DifferentSizeVectorsShouldReturn0(){
        Vector vector = new Vector();
        Vector vector1 = new Vector();

        vector.add(-1);
        vector1.add(-1);
        vector1.add(3);

        Assert.assertEquals(0,vector.compareTo(vector1),0);
    }





    @Test
    public void givenAPointX1Y1AhouldReturnX1YMinus1WhenReverseIsCalled(){

    }
    @Test
    public void shouldReturnX1YMinus1WhenReverseIsCAlledWithX1Y1(){
        //Given


        // When

        // Then

    }
}
