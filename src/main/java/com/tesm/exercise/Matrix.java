package com.tesm.exercise;

import java.util.ArrayList;
import java.util.List;


public class Matrix extends RecognitionBlock<Matrix>{

    protected List<List<Integer>> matrix=new ArrayList<>();


    public Matrix(String src,int startX, int startY,int x,int y) throws InvalidInputException {
        super(startX, startY, x, y);
        int line = startX;

        for (int j=0;j<startY;j++){
            line+=nextLine(src);
        }


        for (int i=0;i<y;i++) {
            matrix.add(getLine(src.substring(line, line+x)));
            line+=nextLine(src);
        }



        for (int i=0;i<matrix.get(0).size();i++){
            Vector vector = new Vector();

            for (int j=0;j<matrix.size();j++){
                vector.add(matrix.get(j).get(i));
            }
            vectors.add(vector);
        }
    }

    public Matrix(List<Vector> vectorList){
        super(vectorList);

        for (int i=0;i<vectorList.size();i++){
            matrix.add(vectorList.get(i).value());
        }
    }

    private List<Integer> getLine(String vect){
        List<Integer> res = new ArrayList<>();
        for (int i=0;i<vect.length();i++){
            if (vect.charAt(i)==' ')
                res.add(0);
            else if (vect.charAt(i)=='+'){
                res.add(1);
            }
        }
        return res;
    }

    private int nextLine(String val){
        return val.indexOf('\n')+1;
    }

    public List<Vector> value(){
        return vectors;
    }

    public Matrix reverseX(){
        List<Vector> vectorList = new ArrayList<>();
        for (Vector vector : vectors){
            vectorList.add(vector.reverse());
        }
        return new Matrix(vectorList);
    }

    public Matrix reverseY(){
        List<Vector> vectorList = new ArrayList<>();
        for(int i=vectors.size()-1;i>=0;i--){
            vectorList.add(vectors.get(i));
        }
        return new Matrix(vectorList);
    }

    public Matrix rotateSx(){
        List<Vector> vectorList = new ArrayList<>();
        for (int j=0;j<vectors.get(0).size();j++) {
            Vector vector = new Vector();
            for (int k=0;k<vectors.size();k++)
                vector.add(vectors.get(k).value().get(j));
            vectorList.add(vector);
        }
        return new Matrix(vectorList);
    }

    public String toString(){
        String val="";

        for (int j=0;j<vectors.get(0).size();j++) {
            for (int k=0;k<vectors.size();k++) {
                val += (vectors.get(k).value().get(j));
            }
            val+='\n';
        }

        return val;
    }

    public double compareTo(Matrix matrix) {
        double res = 0;
        int i =0;
        int isNaN=0;


        for (Vector vector : vectors){
            double val = vector.compareTo(matrix.value().get(i));
            if (val==-1){
                isNaN++;
            }else {
                res += val;
            }
            i++;
        }
        res=100*res/(vectors.size()-isNaN);
        return res;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof Matrix) {
            if ((getY()==(((Matrix) o).getY()))&&(getX()==(((Matrix) o).getX()))
                    &&(getHeight()==(((Matrix) o).getHeight()))&&(getWidth()==(((Matrix) o).getWidth()))) {
                return true;
            }
        }
        return false;
    }
}
