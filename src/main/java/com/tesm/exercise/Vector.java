package com.tesm.exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Vector {
    private List<Integer> vect = new ArrayList<>();

    public Vector() {
    }

    public Vector(List<Integer> integerList) {
        for (Integer i : integerList){
            vect.add(i);
        }
    }

    public void add(int e){
        vect.add(e);
    }

    public int size(){
        return vect.size();
    }


    public List<Integer> value() {
       return vect;
    }


    public Vector reverse(){
        Vector res = new Vector();
        for (int i=vect.size()-1;i>=0;i--){
            res.add(vect.get(i));
        }
        return res;
    }

    public double compareTo(Vector vector) {
        return calculateSimilarity(vect,vector.value());
    }

    private  double calculateDotProduct(List<Integer> vector, List<Integer> vector1){
        double res = 0;
        if (vector.size()!=vector1.size())
            return res;

        for (int i=0;i<vector.size();i++){
            res+=vector.get(i)*vector1.get(i);
        }
        return res;
    }

    private  double calculateSimilarity(List<Integer> vector,List<Integer> vector1){
        double res = 0;
        if (vector.size()!=vector1.size())
            return res;

        if ((vectorNorm(vector)*vectorNorm(vector1))==0)
            return -1;
        res=(calculateDotProduct(vector, vector1)/(vectorNorm(vector)*vectorNorm(vector1)));
        return res;
    }

    private double vectorNorm(List<Integer> vector){
        double res = 0;
        for (int i : vector){
            res+=Math.pow(i,2);
        }

        return Math.sqrt(res);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vector)) return false;
        Vector vector = (Vector) o;
        return Objects.equals(vect, vector.vect);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vect);
    }
}
