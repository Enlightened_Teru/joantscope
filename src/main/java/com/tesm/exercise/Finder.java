package com.tesm.exercise;

import java.util.ArrayList;
import java.util.List;


public class Finder {
    private String map;
    private List<Entity> possibleResults;
    private int x;
    private int y;


    public Finder(String map,int x,int y){
        this.map=map;
        this.x=x;
        this.y=y;
        possibleResults=new ArrayList<>();
    }

    public void search(double minPercentage,Entity item){
        int currentY=0;

        while (currentY<y-item.getHeight()) {
            horizontalSearch(minPercentage,currentY, item);
            currentY+=1;
        }
    }

    private <T extends Entity> void horizontalSearch(double minPercentage,int currentY,T item){
        int currentX=0;
        int i=0;
        Entity previous=null;

        while (currentX<x) {
            Entity area = null;
            try {
                area = item.createNewInstance(new Matrix(map,currentX,currentY,item.getWidth(),item.getHeight()));
            } catch (InvalidInputException e) {
                e.printStackTrace();

            }

            if (area.compareTo(item)>=minPercentage && !possibleResults.contains(area)){
                area.setPercentage(area.compareTo(item));
                possibleResults.add(area);
            }
            currentX+=1;
        }
    }

    public void search(double minPercentage,List<Entity> set){

        for (Entity entity : set){
            search(minPercentage,entity);
        }
    }

    public List<Entity> getPossibleResults() {
        return possibleResults;
    }


}
