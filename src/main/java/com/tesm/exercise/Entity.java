package com.tesm.exercise;

import java.util.Objects;


public abstract class Entity<T extends Entity<?>>{
    protected RecognitionBlock matrix;
    private double percentage;


    public Entity(RecognitionBlock matrix){
        this.matrix = matrix;
    }


    public abstract T rotateSx();

    public abstract T reverseX();

    public abstract T reverseY();

    public void setPercentage(double percentage){
        this.percentage = percentage;
    }

    public double compareTo(T matrix1){
        return matrix.compareTo(matrix1.matrix);
    }

    @Override
    public String toString() {
        return matrix.toString();
    }


    public int getWidth(){
        return matrix.getWidth();
    }

    public int getHeight(){
        return matrix.getHeight();
    }

    public int getX(){
        return matrix.getX();
    }

    public int getY(){
        return matrix.getY();
    }

    public int getPercentage() {
        return (int) percentage;
    }

    public abstract T createNewInstance(RecognitionBlock matrix);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Entity)) return false;
        Entity<?> entity = (Entity<?>) o;
        return Objects.equals(matrix, entity.matrix);
    }

    @Override
    public int hashCode() {
        return Objects.hash(matrix);
    }



}