package com.tesm.exercise;


import java.util.ArrayList;
import java.util.List;


public abstract class RecognitionBlock<T> {
    protected final int x;
    protected final int y;
    protected final int startX;
    protected final int startY;
    protected List<Vector> vectors;

    public RecognitionBlock(int startX, int startY,int x,int y) throws InvalidInputException {
        if (startX<0||startY<0||x<=0||y<=0)
            throw new InvalidInputException();
        this.x=x;
        this.y=y;
        this.startX=startX;
        this.startY=startY;
        this.vectors = new ArrayList<>();
    }

    public RecognitionBlock(List<Vector> vectorList){
        vectors = vectorList;
        //TODO check vectorList size if element 0 is null
        this.x = vectorList.size();
        this.y = vectorList.get(0).size();
        this.startX = 0;
        this.startY = 0;
    }

    abstract double compareTo(T block);

    public int getX(){
        return startX;
    }


    public int getY(){
        return startY;
    }

    public int getWidth() {
        return x;
    }

    public int getHeight() {
        return y;
    }

    abstract T reverseX();
    abstract T reverseY();
    abstract T rotateSx();

    List<Vector> value() {
        return vectors;
    }

}
