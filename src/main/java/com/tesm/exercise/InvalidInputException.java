package com.tesm.exercise;


class InvalidInputException extends Exception {
    public InvalidInputException() {
        super("The value inputed were incorrect, the value must be positive greater than 0.");
    }
}
