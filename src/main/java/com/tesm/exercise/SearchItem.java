package com.tesm.exercise;

public class SearchItem<T extends RecognitionBlock>  extends Entity<SearchItem<T>>{
    private String name;



    public SearchItem(T matrix,String name){
        super(matrix);
        this.name = name;
    }

    @Override
    public String toString() {
        return name+" ("+getX()+","+getY()+")["+getPercentage()+"%]:" +
                "\n"+super.toString()+"\n";
    }

    @Override
    public SearchItem<T> rotateSx() {
        return new SearchItem((T) matrix.rotateSx(),name);
    }

    @Override
    public SearchItem<T> reverseX() {
        return new SearchItem((T) matrix.reverseX(),name);
    }

    @Override
    public SearchItem<T> reverseY() {
        return new SearchItem((T) matrix.reverseY(),name);
    }

    @Override
    public SearchItem createNewInstance(RecognitionBlock matrix) {
        return new SearchItem(matrix,name);
    }
}

