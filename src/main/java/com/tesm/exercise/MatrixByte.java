package com.tesm.exercise;


public class MatrixByte extends RecognitionBlock<MatrixByte> {



    public MatrixByte(int startX, int startY, int x, int y) throws InvalidInputException {
        super(startX, startY, x, y);

    }

    @Override
    double compareTo(MatrixByte block) {
        return 0;
    }

    @Override
    MatrixByte reverseX() {
        return null;
    }

    @Override
    MatrixByte reverseY() {
        return null;
    }

    @Override
    MatrixByte rotateSx() {
        return null;
    }


}
