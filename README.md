Joantscope Data Analysis

It is the year 2087 and the world is at intergalactic war. Your planet, asteroid

X2830-V-3D, is under attack by the evil Caprese who are shooting missiles from

their spaceships. Your task is to detect where the spaceships, missiles and

torpedoes are.

The Joantscope is a special tool which can read the galaxy, but it is not 100%

accurate and the enemy can use cloaking shields to hide some of its

coordinates.

You have the following resources:

1. image.jsc – a 100x100 swatch of raw Joantscope data containing

between four and ten targets

2. missile.jsc – a perfect image of a Caprese missile

3. spaceship.jsc – a perfect image of a Caprese spaceship

The task is to analyse the Joantscope image, and return a list of targets found.

Each target found should include the target type (spaceship or missile), the

coordinates of the target on the image, and some indication of your

confidence in the target detection.